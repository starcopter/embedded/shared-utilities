ifeq (${CI}, true)
  GIT_COMMIT = ${CI_COMMIT_SHA}
  GIT_CLEAN = 1
  BUILD_DATE = $(shell date --date='${CI_COMMIT_TIMESTAMP}' +%s)
else
  GIT_COMMIT = $(shell git rev-parse HEAD)
  GIT_CLEAN = $(shell [[ -z `git status -s` ]] && echo 1 || echo 0)
  BUILD_DATE = $(shell git log -1 --pretty=%ct)
endif
GIT_COMMIT_ARR = $(shell python -c "print(r'{' + ', '.join('0x' + '${GIT_COMMIT}'[i:i+2] for i in range(0, 40, 2)) + r'}', end='')")
export SOURCE_DATE_EPOCH = $(BUILD_DATE)
