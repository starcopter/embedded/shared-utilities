/**
 * @file stm32g4xx-extra.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief CMSIS Device Header Extensions
 * @version 0.2
 * @date 2021-08-16
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @details This file contains constants and macros that somehow have not made their way into ST's CMSIS header (yet).
 */

#pragma once

#include "stm32g4xx.h"

#define FLASH_KEY1 0x45670123U  // Flash key1
#define FLASH_KEY2 0xCDEF89ABU  // Flash key2: used with FLASH_KEY1 to unlock the FLASH registers access

#define FLASH_PDKEY1 0x04152637U  // Flash power down key1
#define FLASH_PDKEY2 0xFAFBFCFDU  // Flash power down key2: used with FLASH_PDKEY1 to unlock the RUN_PD bit in FLASH_ACR

#define FLASH_OPTKEY1 0x08192A3BU  // Flash option byte key1
#define FLASH_OPTKEY2 0x4C5D6E7FU  // Flash option byte key2: used with FLASH_OPTKEY1 to allow option bytes operations

#if defined(STM32G431xx) || defined(STM32G441xx) || defined(STM32G491xx) || defined(STM32G4A1xx)
#    define PAGE_SIZE 2048
#else
#    warning "PAGE_SIZE not set"
#endif

typedef struct {
    __I uint32_t WORD1;
    __I uint32_t WORD2;
    __I uint32_t WORD3;
} UID_TypeDef;

#define UID ((UID_TypeDef*) UID_BASE)

#if defined(STM32G431xx)
#    define TS_CAL1                                                                                                    \
        (*(const uint16_t*) 0x1FFF75A8) /* TS ADC raw data acquired at a temperature of 30 °C (± 5 °C),             \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL1_TEMP (30)           /* 30 °C (± 5 °C), VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL2                                                                                                    \
        (*(const uint16_t*) 0x1FFF75CA) /* TS ADC raw data acquired at a temperature of 130 °C (± 5 °C),            \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL2_TEMP   (110)        /* 110 °C (± 5 °C), VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL_VREF_mV (3000)       /* VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define VREFINT_CAL                                                                                                \
        (*(const uint16_t*) 0x1FFF75AA) /* Raw data acquired at a temperature of 30 °C (± 5 °C),                    \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#elif defined(STM32G441xx) || defined(STM32G491xx)
#    define TS_CAL1                                                                                                    \
        (*(const uint16_t*) 0x1FFF75A8) /* TS ADC raw data acquired at a temperature of 30 °C (± 5 °C),             \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL1_TEMP (30)           /* 30 °C (± 5 °C), VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL2                                                                                                    \
        (*(const uint16_t*) 0x1FFF75CA) /* TS ADC raw data acquired at a temperature of 130 °C (± 5 °C),            \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL2_TEMP   (130)        /* 130 °C (± 5 °C), VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define TS_CAL_VREF_mV (3000)       /* VDDA = VREF+ = 3.0 V (± 10 mV) */
#    define VREFINT_CAL                                                                                                \
        (*(const uint16_t*) 0x1FFF75AA) /* Raw data acquired at a temperature of 30 °C (± 5 °C),                    \
                                           VDDA = VREF+ = 3.0 V (± 10 mV) */
#else
#    warning "Analog calibration values not set"
#endif

// Bit Banding, as specified in PM0214 Rev 10, page 32, section 2.2.5 "Bit-banding"

#define _BIT_BAND_ADDR(region_base, alias_base, word_addr, bit)                                                        \
    ((alias_base) + 32UL * ((uint32_t) ((uint8_t*) (word_addr)) - (region_base)) + 4UL * ((uint32_t) (bit)))

#if defined(PERIPH_BASE) && defined(PERIPH_BB_BASE)
#    define PERIPH_BIT(byte, bit) (*(volatile uint32_t*) _BIT_BAND_ADDR(PERIPH_BASE, PERIPH_BB_BASE, &(byte), bit))
#endif

#if defined(SRAM_BASE) && defined(SRAM_BB_BASE)
#    define SRAM_BIT(byte, bit) (*(volatile uint32_t*) _BIT_BAND_ADDR(SRAM_BASE, SRAM_BB_BASE, &(byte), bit))
#endif

// FDCAN Message RAM, RM0440 Rev 7 p. 1952 "44.3.3 Message RAM"

/**
 * Borrowed from stm32g4xx_hal_fdcan.c; licensed by ST under BSD 3-Clause license.
 *
 * @addtogroup FDCAN_Private_Constants
 * @{
 */

#define FDCAN_TX_EVENT_FIFO_MASK (FDCAN_IR_TEFL | FDCAN_IR_TEFF | FDCAN_IR_TEFN)
#define FDCAN_RX_FIFO0_MASK      (FDCAN_IR_RF0L | FDCAN_IR_RF0F | FDCAN_IR_RF0N)
#define FDCAN_RX_FIFO1_MASK      (FDCAN_IR_RF1L | FDCAN_IR_RF1F | FDCAN_IR_RF1N)
#define FDCAN_ERROR_MASK         (FDCAN_IR_ELO | FDCAN_IR_WDI | FDCAN_IR_PEA | FDCAN_IR_PED | FDCAN_IR_ARA)
#define FDCAN_ERROR_STATUS_MASK  (FDCAN_IR_EP | FDCAN_IR_EW | FDCAN_IR_BO)

#define FDCAN_ELEMENT_STDID_Pos (18U)                                     /* Bits 28:18 */
#define FDCAN_ELEMENT_STDID_Msk (0x7ffUL << FDCAN_ELEMENT_STDID_Pos)      /* 0x1FFC0000 */
#define FDCAN_ELEMENT_STDID     FDCAN_ELEMENT_STDID_Msk                   /* Standard Identifier          */
#define FDCAN_ELEMENT_EXTID_Pos (0U)                                      /* Bits 28:0 */
#define FDCAN_ELEMENT_EXTID_Msk (0x1FFFFFFFUL << FDCAN_ELEMENT_EXTID_Pos) /* 0x1FFFFFFF */
#define FDCAN_ELEMENT_EXTID     FDCAN_ELEMENT_EXTID_Msk                   /* Extended Identifier          */
#define FDCAN_ELEMENT_RTR_Pos   (29U)                                     /* Bit 29 */
#define FDCAN_ELEMENT_RTR_Msk   (0x1UL << FDCAN_ELEMENT_RTR_Pos)          /* 0x20000000 */
#define FDCAN_ELEMENT_RTR       FDCAN_ELEMENT_RTR_Msk                     /* Remote Transmission Request  */
#define FDCAN_ELEMENT_XTD_Pos   (30U)                                     /* Bit 30 */
#define FDCAN_ELEMENT_XTD_Msk   (0x1UL << FDCAN_ELEMENT_XTD_Pos)          /* 0x40000000 */
#define FDCAN_ELEMENT_XTD       FDCAN_ELEMENT_XTD_Msk                     /* Extended Identifier          */
#define FDCAN_ELEMENT_ESI_Pos   (31U)                                     /* Bit 31 */
#define FDCAN_ELEMENT_ESI_Msk   (0x1UL << FDCAN_ELEMENT_ESI_Pos)          /* 0x80000000 */
#define FDCAN_ELEMENT_ESI       FDCAN_ELEMENT_ESI_Msk                     /* Error State Indicator        */
#define FDCAN_ELEMENT_TS_Pos    (0U)                                      /* Bits 15:0 */
#define FDCAN_ELEMENT_TS_Msk    (0xFFFFUL << FDCAN_ELEMENT_TS_Pos)        /* 0x0000FFFF */
#define FDCAN_ELEMENT_TS        FDCAN_ELEMENT_TS_Msk                      /* Timestamp                    */
#define FDCAN_ELEMENT_DLC_Pos   (16U)                                     /* Bit 19:16 */
#define FDCAN_ELEMENT_DLC_Msk   (0xFUL << FDCAN_ELEMENT_DLC_Pos)          /* 0x000F0000 */
#define FDCAN_ELEMENT_DLC       FDCAN_ELEMENT_DLC_Msk                     /* Data Length Code             */
#define FDCAN_ELEMENT_BRS_Pos   (20U)                                     /* Bit 20 */
#define FDCAN_ELEMENT_BRS_Msk   (0x1UL << FDCAN_ELEMENT_BRS_Pos)          /* 0x00100000 */
#define FDCAN_ELEMENT_BRS       FDCAN_ELEMENT_BRS_Msk                     /* Bit Rate Switch              */
#define FDCAN_ELEMENT_FDF_Pos   (21U)                                     /* Bit 21 */
#define FDCAN_ELEMENT_FDF_Msk   (0x1UL << FDCAN_ELEMENT_FDF_Pos)          /* 0x00200000 */
#define FDCAN_ELEMENT_FDF       FDCAN_ELEMENT_FDF_Msk                     /* FD Format                    */
#define FDCAN_ELEMENT_EFC_Pos   (23U)                                     /* Bit 23 */
#define FDCAN_ELEMENT_EFC_Msk   (0x1UL << FDCAN_ELEMENT_EFC_Pos)          /* 0x00800000 */
#define FDCAN_ELEMENT_EFC       FDCAN_ELEMENT_EFC_Msk                     /* Event FIFO Control           */
#define FDCAN_ELEMENT_MM_Pos    (24U)                                     /* Bits 31:24 */
#define FDCAN_ELEMENT_MM_Msk    (0xFFUL << FDCAN_ELEMENT_MM_Pos)          /* 0xFF000000 */
#define FDCAN_ELEMENT_MM        FDCAN_ELEMENT_MM_Msk                      /* Message Marker               */
#define FDCAN_ELEMENT_FIDX_Pos  (24U)                                     /* Bit 24 */
#define FDCAN_ELEMENT_FIDX_Msk  (0x7FUL << FDCAN_ELEMENT_FIDX_Pos)        /* 0x7F000000 */
#define FDCAN_ELEMENT_FIDX      FDCAN_ELEMENT_FIDX_Msk                    /* Filter Index                 */
#define FDCAN_ELEMENT_ANMF_Pos  (31U)                                     /* Bit 31 */
#define FDCAN_ELEMENT_ANMF_Msk  (0x1UL << FDCAN_ELEMENT_ANMF_Pos)         /* 0x80000000 */
#define FDCAN_ELEMENT_ANMF      FDCAN_ELEMENT_ANMF_Msk                    /* Accepted Non-matching Frame  */
#define FDCAN_ELEMENT_ET_Pos    (22U)                                     /* Bits 23:22 */
#define FDCAN_ELEMENT_ET_Msk    (0x3UL << FDCAN_ELEMENT_ET_Pos)           /* 0x00C00000 */
#define FDCAN_ELEMENT_ET        FDCAN_ELEMENT_ET_Msk                      /* Event type                   */

#define SRAMCAN_FLS_NBR (28U) /* Max. Filter List Standard Number      */
#define SRAMCAN_FLE_NBR (8U)  /* Max. Filter List Extended Number      */
#define SRAMCAN_RF0_NBR (3U)  /* RX FIFO 0 Elements Number             */
#define SRAMCAN_RF1_NBR (3U)  /* RX FIFO 1 Elements Number             */
#define SRAMCAN_TEF_NBR (3U)  /* TX Event FIFO Elements Number         */
#define SRAMCAN_TFQ_NBR (3U)  /* TX FIFO/Queue Elements Number         */

#define SRAMCAN_FLS_SIZE (1U * 4U)  /* Filter Standard Element Size in bytes */
#define SRAMCAN_FLE_SIZE (2U * 4U)  /* Filter Extended Element Size in bytes */
#define SRAMCAN_RF0_SIZE (18U * 4U) /* RX FIFO 0 Elements Size in bytes      */
#define SRAMCAN_RF1_SIZE (18U * 4U) /* RX FIFO 1 Elements Size in bytes      */
#define SRAMCAN_TEF_SIZE (2U * 4U)  /* TX Event FIFO Elements Size in bytes  */
#define SRAMCAN_TFQ_SIZE (18U * 4U) /* TX FIFO/Queue Elements Size in bytes  */

/* Filter List Standard Start Address */
#define SRAMCAN_FLSSA ((uint32_t) 0)
/* Filter List Extended Start Address */
#define SRAMCAN_FLESA ((uint32_t) (SRAMCAN_FLSSA + (SRAMCAN_FLS_NBR * SRAMCAN_FLS_SIZE)))
/* Rx FIFO 0 Start Address */
#define SRAMCAN_RF0SA ((uint32_t) (SRAMCAN_FLESA + (SRAMCAN_FLE_NBR * SRAMCAN_FLE_SIZE)))
/* Rx FIFO 1 Start Address */
#define SRAMCAN_RF1SA ((uint32_t) (SRAMCAN_RF0SA + (SRAMCAN_RF0_NBR * SRAMCAN_RF0_SIZE)))
/* Tx Event FIFO Start Address */
#define SRAMCAN_TEFSA ((uint32_t) (SRAMCAN_RF1SA + (SRAMCAN_RF1_NBR * SRAMCAN_RF1_SIZE)))
/* Tx FIFO/Queue Start Address */
#define SRAMCAN_TFQSA ((uint32_t) (SRAMCAN_TEFSA + (SRAMCAN_TEF_NBR * SRAMCAN_TEF_SIZE)))
/* Message RAM size */
#define SRAMCAN_SIZE ((uint32_t) (SRAMCAN_TFQSA + (SRAMCAN_TFQ_NBR * SRAMCAN_TFQ_SIZE)))

/**
 * @}
 *
 * End of borrow.
 */

typedef struct __attribute__((packed, aligned(4))) FDCANExtIDFilterElement {
    union {
        uint32_t word1;
        struct {
            uint32_t EFID1 : 29;  //!< First ID of extended ID filter element
            enum {
                eEFEC_DISABLE       = 0b000,  //!< Disable filter element
                eEFEC_TO_RXFIFO0    = 0b001,  //!< Store in Rx FIFO 0 if filter matches
                eEFEC_TO_RXFIFO1    = 0b010,  //!< Store in Rx FIFO 1 if filter matches
                eEFEC_REJECT        = 0b011,  //!< Reject ID if filter matches
                eEFEC_HP            = 0b100,  //!< Set high priority if filter matches
                eEFEC_TO_RXFIFO0_HP = 0b101,  //!< Set high priority and store in FIFO 0 if filter matches
                eEFEC_TO_RXFIFO1_HP = 0b110,  //!< Set high priority and store in FIFO 1 if filter matches
            } EFEC : 3;                       //!< Extended filter element configuration
        };
    };

    union {
        uint32_t word2;
        struct {
            uint32_t EFID2  : 29;  //!< Second ID of extended ID filter element
            uint32_t _pad_1 : 1;
            enum {
                eEFT_RANGE         = 0b00,  //!< Range filter from FilterID1 to FilterID2
                eEFT_DUAL          = 0b01,  //!< Dual ID filter for FilterID1 or FilterID2
                eEFT_MASK          = 0b10,  //!< Classic filter: FilterID1 = filter, FilterID2 = mask
                eEFT_RANGE_NO_EIDM = 0b11,  //!< Range filter from FilterID1 to FilterID2, EIDM mask not applied
            } EFT : 2;                      //!< Extended filter element configuration
        };
    };
} FDCANExtIDFilterElement;

typedef struct __attribute__((packed, aligned(4))) FDCANRxFIFOElement {
    union {
        uint32_t word1;
        struct {
            uint32_t identifier : 29;  //!< CAN identifier, standard or extended
            uint32_t RTR        : 1;   //!< Remote transmission request
            uint32_t XTD        : 1;   //!< extended identifier
            uint32_t ESI        : 1;   //!< error state indicator
        };
    };

    union {
        uint32_t word2;
        struct {
            uint32_t RXTS  : 16;  //!< Rx timestamp
            uint32_t DLC   : 4;   //!< data length code
            uint32_t BRS   : 1;   //!< bit rate switch
            uint32_t FDF   : 1;   //!< FD format
            uint32_t _res2 : 2;
            uint32_t FIDX  : 7;  //!< filter index
            uint32_t ANMF  : 1;  //!< accepted non-matching frame
        };
    };

    uint8_t data_bytes[64];  ///!< payload, length depends on DLC
} FDCANRxFIFOElement;

typedef struct __attribute__((packed, aligned(4))) FDCANTxBufferElement {
    union {
        uint32_t word1;
        struct {
            uint32_t identifier : 29;
            uint32_t RTR        : 1;
            uint32_t XTD        : 1;
            uint32_t ESI        : 1;
        };
    };

    union {
        uint32_t word2;
        struct {
            uint32_t _res1 : 16;
            uint32_t DLC   : 4;
            uint32_t BRS   : 1;
            uint32_t FDF   : 1;
            uint32_t _res2 : 1;
            uint32_t EFC   : 1;
            uint32_t MM    : 8;
        };
    };

    uint8_t data_bytes[64];
} FDCANTxBufferElement;

typedef struct __attribute__((packed, aligned(4))) FDCANMessageRAM {
    volatile uint32_t                FLS[SRAMCAN_FLS_NBR][SRAMCAN_FLS_SIZE / 4];  //!< Filter List Standard
    volatile FDCANExtIDFilterElement FLE[SRAMCAN_FLE_NBR];                        //!< Filter List Extended
    volatile FDCANRxFIFOElement      RF0[SRAMCAN_RF0_NBR];                        //!< RX FIFO 0
    volatile FDCANRxFIFOElement      RF1[SRAMCAN_RF1_NBR];                        //!< RX FIFO 1
    volatile uint32_t                TEF[SRAMCAN_TEF_NBR][SRAMCAN_TEF_SIZE / 4];  //!< TX Event FIFO
    volatile FDCANTxBufferElement    TFQ[SRAMCAN_TFQ_NBR];                        //!< TX FIFO/Queue
} FDCANMessageRAM;

// Expected CREL (Core Release Register) value
#define FDCAN_CREL_EXPECT 0x32141218UL
