/**
 * @file transport.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN Transport Layer Abstraction and Worker Task
 * @version 0.1
 * @date 2022-01-09
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * =====================================================================================================================
 *
 * This module provides a UAVCAN/CAN (via libcanard) transport handler task, with mechanisms for asynchronous handling
 * of the bus-side functions (frame reception and transmission) required by libcanard. It defines a media-agnostic
 * interface, by which different CAN interface drivers may be connected.
 *
 * An application is not expected to interact with the transport module directly, but rather utilize the accompanying
 * (task-less) node module handling the application side of libcanard.
 *
 */

#pragma once

#include <main.h>

#include <FreeRTOS.h>
#include <task.h>

#include "node.h"

// =========================================== MODULE LEVEL CONFIGURATION ==============================================

#ifndef configTRANSPORT_TASK_PRIORITY
#    define configTRANSPORT_TASK_PRIORITY (configMAX_PRIORITIES - 1)
#endif

#ifndef configTRANSPORT_STACK_SIZE
#    define configTRANSPORT_STACK_SIZE 256
#endif

#ifndef configTRANSPORT_BLOCK_TIME_MS
#    define configTRANSPORT_BLOCK_TIME_MS 1000
#endif

#ifndef configTRANSPORT_LOG_LEVEL
#    define configTRANSPORT_LOG_LEVEL LOG_LEVEL_TRACE
#endif

static_assert(configNODE_NUM_INTERFACES <= 3, "UAVCAN supports up to triply modular redundant interfaces, not more");

// ================================== EVENT FLAG DECLARATION / TASK NOTIFICATIONS ======================================

// The transport worker task expects to receive notifications about interface events as FreeRTOS Task Notification [1].
// When an interface has e.g. received a frame, the transport worker task should be notified from the interface's ISR:
//
//     BaseType_t should_yield = pdFALSE;
//     xTaskNotifyFromISR(transport_task_handle, TRANSPORT_FRAME_RECEIVED(index), eSetBits, &should_yield);
//     portYIELD_FROM_ISR(should_yield);
//
// FreeRTOS' Task Notifications are used as an event group in this situation, as detailled in [2]. Separate events are
// assigned different flags (bits in the notification value); setting flags in the notification is an interrupt-safe
// atomic operation, a changed notification value unblocks the transport task, and flags are cleared atomically on read.
// Should an ISR (or anything else, for that matter) set multiple flags before the transport handler is scheduled to run
// again, the existing flags will not be overwritten, and the transport worker task will handle all events together,
// though not necessary in order of occurrence: if in short succession the secondary and afterwards the primary
// interface receive a frame, the primary interface will be polled first.
//
// [1]: https://www.freertos.org/RTOS-task-notifications.html
// [2]: https://www.freertos.org/RTOS_Task_Notification_As_Event_Group.html

// interface has received a frame and should be polled via MediaIOInterface::pop_frame()
#define TRANSPORT_FRAME_RECEIVED(redundant_transport_index) (0x01UL << (8 * (redundant_transport_index)))
// interface has successfully transmitted a frame and is ready for another one (via MediaIOInterface::push_frame())
#define TRANSPORT_FRAME_TRANSMITTED(redundant_transport_index) (0x02UL << (8 * (redundant_transport_index)))
// interface's controller error state has changed, the new state may be polled via MediaIOInterface::get_stats()
#define TRANSPORT_BUS_STATE_CHANGED(redundant_transport_index) (0x04UL << (8 * (redundant_transport_index)))

// =============================================== TYPE DECLARATIONS ===================================================

/**
 * @brief Abstract Bus Error State.
 *
 * The BusErrorStats type describes the state of a single CAN (FD) controller. This is the type the interface function
 * MediaIOInterface::get_stats() returns.
 *
 * The interface still is quite unstable, since so far only the STM32G4 Series FDCAN interface has been taken into
 * account for this struct. Once support for the H7's FDCAN interface and the bxCAN interfaces of the F and L series
 * gets implemented, more information or different abstraction levels may get added or modified.
 */
typedef struct __packed BusErrorStats {
    // CAN Controller Receive Error Counter (REC)
    // The REC is incremented by 1 (or 8, in some situations) when the receiver detects an error, and decremented by 1
    // upon successful frame reception. With REC > 95 the controller enters Error Active (Warning), with REC > 127 it
    // enters Error Passive state, given the TEC does not cause a higher error state first.
    uint8_t receive_error_counter;

    // CAN Controller Transmit Error Counter (TEC)
    // The TEC is incremented by 8 when the controller detects an error it may have caused or contributed to (roughly),
    // it is decremented by 1 for each successful frame transmission. With TEC > 95 the controller enters Error Active
    // (Warning), with TEC > 127 it enters Error Passive state, given the REC does not cause a higher error state first.
    // Should the Transmit Error Counter exceed 255, the controller is forced into Bus Off State and cannot participate
    // in bus communications before being restarted and having detected 128 occurrences of 11 consecutive recessive
    // bits.
    uint8_t transmit_error_counter;

    // CAN Controller Error State Machine State
    enum CANErrorState {
        // Error Active
        // The controller is in normal operation and will send an active error frame when it detects a bus error.
        eBusErrorActive = 0,
        // Error Active (Warning)
        // The controller is still in normal operation, but REC or TEC have reached 96.
        eBusErrorWarning = 1,
        // Error Passive
        // The controller is in degraded operational state and may only send passive error frames upon detecting errors.
        // Either REC or TEC have reached 128.
        eBusErrorPassive = 2,
        // Bus Off
        // The TEC has exceeded 255, causing the controller to disconnect from the bus.
        // No frames can be transmitted or received before resetting the controller.
        eBusErrorBusOff = 3
    } state : 4;

    uint8_t error_warning_flag   : 1;  // controller in Error Active (Warning) State, REC or TEC have reached 96
    uint8_t error_passive_flag   : 1;  // controller in Error Passive State, REC or TEC have reached 128
    uint8_t bus_off_flag         : 1;  // controller in Bus Off State, TEC has exceeded 255
    uint8_t _pad_1               : 1;
    uint8_t last_error_code      : 3;  // STM32G4 FDCAN_PSR LEC[2:0], see RM0440 for Coding
    uint8_t _pad_2               : 1;
    uint8_t data_last_error_code : 3;  // STM32G4 FDCAN_PSR DLEC[2:0], see RM0440 for Coding
    uint8_t _pad_3               : 1;
} BusErrorStats;

/**
 * @brief Media IO Layer Abstraction Interface for Transport Worker Task Integration.
 *
 * This is as close as C gets to object-oriented design: a MediaIOInterface object (or a reference to one), holding
 * function pointers to interact with one specific IO interface, can be passed to the transport worker task to enable
 * usage of media- and platform-specific logic in the worker task. Any driver implementing the interface specified in
 * the MediaIOInterface type can be selected (and possibly configured) at runtime.
 * Other than with a C++ class, a MediaIOInterface "instance" has neither a self-reference nor any internal state.
 * Therefore all function pointers have to refer to partial functions which are already pre-configured for a specific
 * interface/channel, should more than one exist.
 *
 * None of the function pointers are allowed to be NULL; should an interface not implement e.g. hardware acceptance
 * filtering, its driver needs to provide an empty stub function for the MediaIOInterface::set_filter() member.
 * THE TRANSPORT WORKER DOES NOT CHECK A FUNCTION POINTER'S TARGET ADDRESS BEFORE EXECUTING IT.
 */
typedef struct MediaIOInterface {
    /**
     * @brief Configure Interface Interrupts for Transport Layer Integration
     *
     * The transport worker task expects asynchronous event notifications from interfaces via FreeRTOS Task
     * Notifications (see event flag declarations in transport.h). This requires an interface to be aware of both the
     * (transport worker) task handle to send notifications to, and the configured redundant transport index (which may
     * be different from the platform's hardware enumeration) to send notifications for.
     *
     * This function gets called by the transport handler task, to register and unregister the task handle and index
     * with the interface. It may also be used to e.g. configure/enable/disable the interface interrupt, or perform
     * other initialization tasks.
     *
     * An interface interrupt service routine has to be able to call FreeRTOS API functions, it therefore must not
     * exceed configMAX_SYSCALL_INTERRUPT_PRIORITY. See [1] for more details on this topic.
     *
     * Upon registering an interface to the transport handler, this function will get called with the transport task
     * handle and configured interface index. When unregistering an interface from the transport handler, this function
     * will get called with a NULL notification target and the previously configured index. After this unregister call,
     * the interface must not use the task handle and index for further task notifications.
     *
     * [1]: https://www.freertos.org/a00110.html#kernel_priority
     *
     * @param notification_target handle of transport task to notify of events, NULL to disable/unregister
     * @param redundant_transport_index interface index to send notifications for, may differ from hardware index
     *
     * @see NVIC_EnableIRQ, NVIC_SetPriority, configMAX_SYSCALL_INTERRUPT_PRIORITY
     */
    void (*install_hooks)(const TaskHandle_t notification_target, const uint_fast8_t redundant_transport_index);

    /**
     * @brief Push a CanardTxQueueItem to the Media IO Layer for Transmission
     *
     * This function is called by the transport worker to (attempt to) enqueue a CAN frame in the interface's
     * transmission queue. If the frame has been handled an may be discarded (because it has been successfully copied
     * into the transmission queue, because the transmission deadline has been exceeded, or because it has been dropped
     * by another reason) the function shall return true, otherwise it shall return false, causing the transport handler
     * to attempt transmitting the frame at a later time.
     *
     * The interface shall guarantee that frames with identical CAN identifiers are transmitted in order of appearance,
     * as is required by the UAVCAN standard. Furthermore the interface is expected to (try to) avoid blocking the
     * TX queue with a low-priority frame, see section 4.2.4.3 "Inner priority inversion" of the UAVCAN specification
     * for a more detailled discussion on that subject.
     *
     * The interface driver is given a CanardTxQueueItem (containing a transmission deadline and the frame to transmit),
     * and a current reference timestamp to compare the tx deadline to. If possible, the interface shall cancel queued
     * frames once their tx deadline has passed.
     *
     * @param current_time timestamp the interface driver shall compare tx_item->tx_deadline_usec to
     * @param tx_item reference to canard queue item containing the frame and a tx deadline
     *
     * @return true if the frame has been handled and may be discarded,
     *         false if it shall be pushed again at a later time
     */
    bool (*push_frame)(const CanardMicrosecond current_time, const CanardTxQueueItem* const tx_item);

    /**
     * @brief Query Interface for Received Frames
     *
     * After receiving a Frame Received Notification from an interface, the transport handler calls this function to
     * read received frames from the interface's RX queue(s). If a received frame was found, the driver shall populate
     * *out_canard_frame and *out_rx_timestamp with the received frame and timestamp of said reception and return true.
     * If the interface's reception queue is exhausted and no new data is available, this function shall return false.
     * In case no RX timestamping is implemented for the interface, *out_rx_timestamp shall be set zo zero.
     *
     * @param out_canard_frame output buffer to place the received frame in
     * @param out_rx_timestamp output buffer for reception timestamp, shall be set to 0 if not implemented
     * @return true if a frame was copied from the RX queue to *out_canard_frame,
     *         false if the queue is exhausted and no further frames are available
     */
    bool (*pop_frame)(CanardFrame* const out_canard_frame, uint64_t* const out_rx_timestamp);

    /**
     * @brief Configure Hardware Acceptance Filter for Interface
     *
     * This function is executed by the node module (not the transport task) to configure hardware acceptance filters
     * based on node ID and active subscriptions. The driver function is expected to ignore indices which exceed the
     * available filters. A filter configuration with an all-zero mask shall disable the filter.
     *
     * @param filter CanardFilter instance with ID and Mask
     * @param index position in the interface's filter bank index to write this filter to
     */
    void (*set_filter)(const CanardFilter filter, const uint_fast8_t index);

    uint_fast8_t num_filters;  //<! Number of available Hardware Acceptance Filters for Interface

    /**
     * @brief Collect State Metrics from the Interface
     *
     * This function is (or may be) called after receiving a Bus State Changed Notification to collect more in-depth
     * bus stats, controller error counters (REC, TEC), the CAN error state (error active, error passive, etc.) and
     * accumulated bus errors for this interface.
     *
     * The function shall be safe to be called with a NULL argument, in which case only the number of new bus errors
     * shall be returned.
     *
     * @param out_bus_error_state BusErrorStats struct to write the information to,
     *                            or NULL if no detailled report is required
     * @return number of new bus errors since the last call for this interface
     */
    uint_fast8_t (*get_stats)(BusErrorStats* const out_bus_error_stats);
} MediaIOInterface;

// ================================================ TASK-LOCAL STATE ===================================================

uint_fast8_t transport_get_health(void);
uint_fast8_t transport_get_mode(void);
