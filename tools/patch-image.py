import binascii
import struct
from dataclasses import dataclass
from pathlib import Path

from elftools.elf.elffile import ELFFile

FLASH_START = 0x08000000
HEADER_MAGIC = 0x3063737F
VT_ALIGN = 512


@dataclass
class HeaderHeader:
    """Header's header with a fixed format."""

    magic: int
    header_start: int
    vector_table: int
    header_size: int
    image_end: int

    @property
    def image_size(self) -> int:
        return self.image_end - self.header_start

    @classmethod
    def from_bytes(cls, image: bytes):
        if len(image) < 20:
            raise ValueError("Can't parse partial header.")
        return cls(*struct.unpack("<LLLLL", image[:20]))

    def validate(self) -> None:
        """Validate static header attributes.

        Raises:
            ValueError: if the header is somehow malformed
        """
        if self.magic != HEADER_MAGIC:
            raise ValueError(f"Unsupported Image. Expected {HEADER_MAGIC:08x}, got {self.magic:08x}.")
        if self.vector_table & (VT_ALIGN - 1):
            raise ValueError(f"Vector table at 0x{self.vector_table:08x} not aligned to {VT_ALIGN}-Byte boundary.")


def get_symbol_offset(elf: ELFFile, name: str) -> int:
    """Get location of symbol <name> in ELF file."""

    symtab = elf.get_section_by_name(".symtab")
    if not symtab:
        raise AttributeError("No symbol table available! Is the ELF stripped?")

    try:
        (symbol,) = symtab.get_symbol_by_name(name)
    except (TypeError, ValueError) as err:
        raise ValueError(f"The symbol '{name}' does not exist, or the name is not unique.") from err

    file_offset = None
    for seg in elf.iter_segments():
        if seg.header["p_type"] != "PT_LOAD":
            continue
        # If the symbol is inside the range of a LOADed segment, calculate the file
        # offset by subtracting the virtual start address and adding the file offset
        # of the loaded section(s)
        if seg["p_vaddr"] <= symbol["st_value"] < seg["p_vaddr"] + seg["p_filesz"]:
            file_offset = symbol["st_value"] - seg["p_vaddr"] + seg["p_offset"]
            break

    if file_offset is None:
        raise ValueError(f"The symbol '{name}' seems not to be present in this file.")

    assert isinstance(file_offset, int)
    return file_offset


def calculate_checksums(bin_file: Path) -> tuple[int, int]:
    """Calculate Header and Image CRC of binary application image.

    Args:
        bin_file (Path): compiled image in `.bin` (not `.elf`) format

    Raises:
        ValueError: if the header format or file size does not match

    Returns:
        header_crc, image_crc: calculated CRC32 checksums
    """
    image = bin_file.read_bytes()
    header = HeaderHeader.from_bytes(image)

    header.validate()
    if len(image) != header.image_size:
        raise ValueError(f"Image length mismatch: expected {header.image_size}, got {len(image)}")

    header_crc = binascii.crc32(image[: header.header_size - 4])
    image_crc = binascii.crc32(
        image[: header.header_size - 4] + struct.pack("<L", header_crc) + image[header.header_size : -4]
    )

    return header_crc, image_crc


def patch_bin_file(bin_file: Path, header_crc: int = None, image_crc: int = None) -> None:
    """Patch image with checksums.

    Args:
        bin_file (Path): compiled image in `.bin` (not `.elf`) format
        header_crc, image_crc (int | None): pre-compiled checksums, optional

    Raises:
        ValueError: if the header format or file size does not match
    """
    if header_crc is None or image_crc is None:
        header_crc, image_crc = calculate_checksums(bin_file)
    header = HeaderHeader.from_bytes(bin_file.read_bytes())

    print(f"Writing {header_crc=:08x}, {image_crc=:08x} to {bin_file.name}")
    with bin_file.open("r+b") as f:
        f.seek(header.header_size - 4)
        f.write(struct.pack("<L", header_crc))
        f.seek(header.image_size - 4)
        f.write(struct.pack("<L", image_crc))


def patch_elf_file(elf_file: Path, header_crc: int, image_crc: int) -> None:
    """Patch image with checksums.

    Args:
        elf_file (Path): compiled image in ELF format
        header_crc, image_crc (int): pre-compiled checksums
    """
    with elf_file.open("rb") as f:
        elf = ELFFile(f)
        image_hdr_offset = get_symbol_offset(elf, "image_hdr")
        elf.stream.seek(image_hdr_offset)
        header = HeaderHeader.from_bytes(elf.stream.read(20))
        header_crc_offset = image_hdr_offset + header.header_size - 4
        image_crc_offset = get_symbol_offset(elf, "__image_crc")

    print(f"Writing {header_crc=:08x}, {image_crc=:08x} to {elf_file.name}")
    with elf_file.open("r+b") as f:
        f.seek(header_crc_offset)
        f.write(struct.pack("<L", header_crc))
        f.seek(image_crc_offset)
        f.write(struct.pack("<L", image_crc))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=Path, help="file to patch")
    args = parser.parse_args()

    elf_file = Path(args.image).with_suffix(".elf")
    bin_file = Path(args.image).with_suffix(".bin-unpatched")

    header_crc, image_crc = calculate_checksums(bin_file)
    patch_elf_file(elf_file, header_crc, image_crc)
