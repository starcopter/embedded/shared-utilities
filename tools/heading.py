#!/usr/bin/env python


def heading(s: str, lead: str = "// ", width: int = 120, fillchar: str = "=") -> str:
    if len(fillchar) > 1:
        fillchar = fillchar[0]
    n = len(lead)
    return lead + f" {s.upper()} ".center(width - 2 * n, fillchar) + n * fillchar


if __name__ == "__main__":
    import sys

    for s in sys.argv[1:]:
        print(heading(s))
