#!/usr/bin/env python

PRIORITIES = (
    "Exceptional",
    "Immediate",
    "Fast",
    "High",
    "Nominal",
    "Low",
    "Slow",
    "Optional",
)

identifier = 0x1473250A


def decode(identifier: int) -> str:
    priority = (identifier & 0x1C000000) >> 26
    service = bool(identifier & 0x02000000)
    request = anonymous = bool(identifier & 0x01000000)
    subject_id = (identifier & 0x1FFF00) >> 8
    service_id = (identifier & 0x7FC000) >> 14
    destination = (identifier & 0x3F80) >> 7
    source = identifier & 0x7F

    prio = f"priority {priority} ({PRIORITIES[priority]})"

    if service:
        return f"{'Request' if request else 'Response'} {service_id} {source} -> {destination}, {prio}"
    else:
        if anonymous:
            return f"Anonymous Message {subject_id}, {prio}"
        else:
            return f"Message {subject_id} from {source}, {prio}"


if __name__ == "__main__":
    import sys

    for string_identifier in sys.argv[1:]:
        print(decode(int(string_identifier, 16)))
