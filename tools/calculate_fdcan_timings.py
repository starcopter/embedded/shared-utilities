#!/usr/bin/env python3

from collections import namedtuple
import argparse

NBRP_MAX   = 512
NTSEG1_MAX = 256
NTSEG2_MAX = 128
DBRP_MAX   = 32
DTSEG1_MAX = 32
DTSEG2_MAX = 16

# with less than 4 tq per bit we won't be able to reach an acceptable sample point
MIN_TQ_PER_BIT = 4

SAMPLE_POINT_MIN = .75
SAMPLE_POINT_TARGET = .875
SAMPLE_POINT_MAX = .9

Solution = namedtuple("Solution", ["prescaler", "tseg1", "tseg2", "sample_point_error"])

def calculate_timings(peripheral_clock_rate: int, target_bitrate: int, prescaler_max: int, bit_segment_1_max: int, bit_segment_2_max: int):
    print(f"Calculate bit timing for {target_bitrate/1000:.0f} kbit/s")

    clock_cycles_per_bit = peripheral_clock_rate // target_bitrate

    time_quanta_before_sample_point = 1 + bit_segment_1_max
    time_quanta_after_sample_point = int(time_quanta_before_sample_point * (1 - SAMPLE_POINT_MIN) / SAMPLE_POINT_MIN)

    prescaler = 1
    time_quanta_per_bit = int(time_quanta_before_sample_point + (bit_segment_2_max if bit_segment_2_max < time_quanta_after_sample_point else time_quanta_after_sample_point))

    best_solution_so_far = Solution(prescaler, bit_segment_1_max, time_quanta_per_bit-time_quanta_before_sample_point, 1)
    break_reason = None

    while break_reason is None:
        while True:
            clock_cycles = prescaler * time_quanta_per_bit
            if clock_cycles > clock_cycles_per_bit:
                time_quanta_per_bit -= 1
            elif clock_cycles < clock_cycles_per_bit:
                prescaler += 1
            else:
                break

            if prescaler > prescaler_max or time_quanta_per_bit < MIN_TQ_PER_BIT:
                break_reason = "TIME_QUANTUM_ITERATION_END"
                break

        # we've got a candidate, now try setting the sample point
        time_quanta_after_sample_point  = int(time_quanta_per_bit * (1 - SAMPLE_POINT_MAX))
        if time_quanta_after_sample_point > bit_segment_2_max:
            time_quanta_after_sample_point = bit_segment_2_max
        time_quanta_before_sample_point = time_quanta_per_bit - time_quanta_after_sample_point
        if time_quanta_before_sample_point > bit_segment_1_max + 1:
            time_quanta_before_sample_point = bit_segment_1_max + 1
            time_quanta_after_sample_point = time_quanta_per_bit - time_quanta_before_sample_point
        last_sample_point_error = 1

        while True:
            sample_point = time_quanta_before_sample_point / time_quanta_per_bit
            if sample_point < SAMPLE_POINT_MIN:
                # this run failed, try again
                prescaler += 1
                break  # inner loop

            sample_point_error = abs(sample_point - SAMPLE_POINT_TARGET)
            if sample_point_error < best_solution_so_far.sample_point_error:
                best_solution_so_far = Solution(prescaler, time_quanta_before_sample_point-1, time_quanta_after_sample_point, sample_point_error)
            elif sample_point_error > last_sample_point_error:
                prescaler += 1
                break

            if sample_point_error <= 1e-3:
                break_reason = "GOOD_SOLUTION_FOUND"
                break

            last_sample_point_error = sample_point_error
            time_quanta_after_sample_point += 1
            time_quanta_before_sample_point -= 1

    tq_ns = best_solution_so_far.prescaler * 1_000_000_000 / peripheral_clock_rate
    time_quanta_per_bit = best_solution_so_far.tseg1 + best_solution_so_far.tseg2 + 1
    sample_point = (best_solution_so_far.tseg1 + 1) / time_quanta_per_bit
    print(f"prescaler = {best_solution_so_far.prescaler}, "
          f"tq = {tq_ns:6.3f} ns, "
          f"tbit = {time_quanta_per_bit * tq_ns:6.1f} ns "
          f"(1 + {best_solution_so_far.tseg1:3d} + {best_solution_so_far.tseg2:2d} tq), "
          f"SP at {sample_point*100:4.1f} % after {(best_solution_so_far.tseg1 + 1) * tq_ns:5.1f} ns")

    return best_solution_so_far

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Calculate timing configuration for STM32 FDCAN controller.")

    parser.add_argument("nominal_bitrate", type=float, help="bit rate during arbitration phase")
    parser.add_argument("data_bitrate", type=float, help="bit rate during data phase")
    parser.add_argument("--pclk", type=float, default=170_000_000, help="FDCAN peripheral clock")

    args = parser.parse_args()

    nominal_timings = calculate_timings(int(args.pclk), int(args.nominal_bitrate), NBRP_MAX, NTSEG1_MAX, NTSEG2_MAX)
    data_timings = calculate_timings(int(args.pclk), int(args.data_bitrate), DBRP_MAX, DTSEG1_MAX, DTSEG2_MAX)
