/**
 * @file assert.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Custom, Space-Sensitive Assert Implementation
 * @version 0.1
 * @date 2021-05-31
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * Inspired by the Interrupt blog post "Using Asserts in Embedded Systems"
 * https://interrupt.memfault.com/blog/asserts-in-embedded-systems
 *
 * When not using the Memfault-provided ASSERT macro (selected by configUSE_MEMFAULT_ASSERT), this implementation
 * requires a __FILENAME__ macro being set per source file. Other than __FILE__, __FILENAME__ is not added by the
 * compiler by default and has to be created manually in the build system. With GNU Make, this can be done using
 * the $(notdir ...) function:
 *
 *     $(CC) -D__FILENAME__=\"$(notdir $<)\" -c $*.c -o $@
 *
 */
#pragma once

#include <main.h>

#if !defined configUSE_MEMFAULT_ASSERT
#    define configUSE_MEMFAULT_ASSERT 0
#endif

#if configUSE_MEMFAULT_ASSERT
#    include <memfault/panics/assert.h>
#    define ASSERT(expr) MEMFAULT_ASSERT(expr)
#else
void assertion_failed(const char* file, uint32_t line) __attribute__((noreturn));
#    ifndef __FILENAME__
#        warning __FILENAME__ not set by build system, falling back to __FILE__
#        define __FILENAME__ __FILE__
#    endif
#    define ASSERT(expr) ((expr) ? (void) 0U : assertion_failed(__FILENAME__, __LINE__))
#endif

#define WTF() ASSERT(0)

// PM0214 Rev 10, page 226, section 4.4.3 Interrupt control and state register (ICSR)
#define CURRENTLY_RUNNING_IRQn ((IRQn_Type) ((int32_t) (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) - 16))
#define RUNNING_INSIDE_ISR     ((SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0)
#define DEBUGGER_CONNECTED     (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk)
#define HALT()                 __asm("bkpt 1")
#define HALT_IF_DEBUGGING()                                                                                            \
    do {                                                                                                               \
        if (DEBUGGER_CONNECTED) HALT();                                                                                \
    } while (0)
