/**
 * @file flash.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Utilities to Program Internal Flash Memory
 * @version 0.1
 * @date 2021-07-11
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <device.h>
#include "assert.h"
#include "memory_map.h"
#include "flash.h"

#define LOG_LEVEL LOG_LEVEL_TRACE
#include "log.h"

void flash_unlock(void) {
    while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
        // wait for flash to be idle
    }

    FLASH->KEYR = FLASH_KEY1;
    FLASH->KEYR = FLASH_KEY2;
}

void flash_lock(void) {
    while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
        // wait for flash to be idle
    }

    PERIPH_BIT(FLASH->CR, FLASH_CR_LOCK_Pos) = 1;
}

uint32_t flash_get_page_number(const uint32_t* const addr) {
    ASSERT(addr >= &__flash_start__ && addr < &__flash_end__);
    return ((uint32_t) addr - FLASH_BASE) / PAGE_SIZE;
}

static void _flash_reset_errors(void) {
    const uint32_t flash_sr = FLASH->SR;
    if (flash_sr) {
        LOG_DEBUG("flash->SR = 0x%04x", (uint16_t)flash_sr);
        FLASH->SR = flash_sr;  // clear flags
    }
}

void flash_erase_page(uint32_t page) {
    // RM0440 Section 5.3.6 Flash main memory erase sequences: Page erase

    // 1. Check that no Flash memory operation is ongoing.
    while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
    }

    // 2. Check and clear all error programming flags due to a previous programming.
    _flash_reset_errors();

    // 3. Set the PER bit and select the page to erase (PNB).
    FLASH->CR |= (page << FLASH_CR_PNB_Pos) | FLASH_CR_PER;

    // 4. Set the STRT bit in the FLASH_CR register.
    PERIPH_BIT(FLASH->CR, FLASH_CR_STRT_Pos) = 1;

    // 5. Wait for the BSY bit to be cleared in the FLASH_SR register.
    while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
    }

    ASSERT(FLASH->SR == 0UL);

    // clean up
    FLASH->CR &= ~(FLASH_CR_PNB_Msk | FLASH_CR_PER_Msk);
}

void flash_write(uint32_t* dst, uint32_t* src, uint32_t bytes) {
    // RM0440 Section 5.3.7 Flash main memory programming sequences: Standard programming

    ASSERT(dst >= &__flash_start__ && (uint32_t*) ((uint32_t) dst + bytes) <= &__flash_end__);
    ASSERT(((uint32_t) dst & 7UL) == 0);  // make sure *dst is 64-bit aligned
    ASSERT((bytes & 7UL) == 0);           // make sure size is also 64-bit aligned
    uint32_t* const src_end = (uint32_t*) ((uint32_t) src + bytes);

    // 1. Check that no Flash main memory operation is ongoing.
    while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
    }

    // 2. Check and clear all error programming flags due to a previous programming.
    _flash_reset_errors();

    // 3. Set the PG bit in the Flash control register (FLASH_CR).
    PERIPH_BIT(FLASH->CR, FLASH_CR_PG_Pos) = 1;

    while (src < src_end) {
        // 4. Perform the data write operation at the desired memory address, inside main memory block or OTP area.
        //    Only double word can be programmed.
        *dst++ = *src++;
        __ISB();
        *dst++ = *src++;

        // 5. Wait until the BSY bit is cleared in the FLASH_SR register.
        while (PERIPH_BIT(FLASH->SR, FLASH_SR_BSY_Pos)) {
        }

        ASSERT(FLASH->SR == 0UL);

        // 6. Check that EOP flag is set in the FLASH_SR register (meaning that the programming operation has succeed),
        //    and clear it by software.
        //    This bit is set only if the end of operation interrupts are enabled (EOPIE = 1).
    }

    // 7. Clear the PG bit in the FLASH_SR register if there no more programming request anymore.
    PERIPH_BIT(FLASH->CR, FLASH_CR_PG_Pos) = 0;
}
