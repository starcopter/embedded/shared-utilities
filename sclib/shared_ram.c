/**
 * @file shared_ram.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Shared RAM Interface
 * @version 0.1
 * @date 2021-06-23
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <stddef.h>
#include <string.h>

#include <device.h>
#include "shared_ram.h"
#include "memory_map.h"
#include "image.h"
#include "crc.h"

enum InterApplicationCommand { eINTER_APPLICATION_COMMAND_NONE = 0, eINTER_APPLICATION_COMMAND_DFU = 1 };

struct SharedRAM {
    ApplicationContext_t  bootloader_launch_context;
    ApplicationContext_t  app_loader_launch_context;
    const ImageHeader_t*  caller;
    ApplicationContext_t* current_context;

    uint32_t command;
    union Param {
        struct DFU {
            char                 path[256];
            uint32_t             server_node_id;
            const ImageHeader_t* current_application;
        } dfu;
    } param;
    uint32_t crc;
}* sr = (struct SharedRAM*) &__shared_ram_start__;

ApplicationContext_t* shared_get_current_context(void) { return sr->current_context; }

uint32_t shared_get_boot_count(void) { return sr->current_context->boot_count; }

void shared_reset_boot_count(void) { sr->current_context->boot_count = 0; }

void _reset_context_on_boot(void) {
    sr->caller          = NULL;
    sr->current_context = NULL;
}

void _switch_app_context(ApplicationContext_t* new_context) {
    if (sr->current_context != NULL) sr->caller = sr->current_context->application;
    sr->current_context = new_context;
}

ImageHeader_t* shared_get_caller(void) { return (ImageHeader_t*) sr->caller; }

ApplicationContext_t* _bootloader_get_launch_context(void) { return &sr->bootloader_launch_context; }

ApplicationContext_t* _app_loader_get_launch_context(void) { return &sr->app_loader_launch_context; }

void shared_register_dfu_request(const char* file_path, size_t path_length, uint32_t server_node_id) {
    if (path_length >= sizeof(sr->param.dfu.path)) return;
    sr->command = eINTER_APPLICATION_COMMAND_DFU;
    memcpy(sr->param.dfu.path, file_path, path_length);
    sr->param.dfu.path[path_length]   = 0;
    sr->param.dfu.server_node_id      = server_node_id;
    sr->param.dfu.current_application = sr->current_context->application;
    // discard check result, overwrite existing crc
    crc32((uint32_t*) &sr->command, sizeof(sr->command) + sizeof(sr->param), &sr->crc);
}

bool shared_dfu_requested(void) {
    uint32_t crc = sr->crc;
    return sr->command == eINTER_APPLICATION_COMMAND_DFU &&
           crc32((uint32_t*) &sr->command, sizeof(sr->command) + sizeof(sr->param), &crc);
}

int shared_get_dfu_context(char*                 out_file_path,
                           uint32_t*             out_server_node_id,
                           const ImageHeader_t** out_current_application) {
    if (!shared_dfu_requested()) return -1;
    strcpy(out_file_path, sr->param.dfu.path);
    *out_server_node_id      = sr->param.dfu.server_node_id;
    *out_current_application = sr->param.dfu.current_application;
    return strlen(sr->param.dfu.path);
}

void shared_reset_dfu_request(void) {
    if (sr->command != eINTER_APPLICATION_COMMAND_DFU) return;
    sr->command = eINTER_APPLICATION_COMMAND_NONE;
    memset(&sr->param.dfu, 0, sizeof(sr->param.dfu));
    sr->crc = 0;
}
