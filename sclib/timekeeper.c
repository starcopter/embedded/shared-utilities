/**
 * @file timekeeper.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Logic and States to keep a Monotonic Microsecond Clock
 * @version 0.1
 * @date 2021-12-02
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <device.h>
#include "timekeeper.h"

#include "../canard/node.h"
#include "registry.h"

#ifndef configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
#    define configTIMEKEEPER_PUBLISH_STATUS_MESSAGE 1
#endif

#if configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
#    include <starcopter/TimekeeperStatus_0_1.h>
#endif  // configTIMEKEEPER_PUBLISH_STATUS_MESSAGE

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

static volatile uint64_t monotonic_timestamp_carry     = 0;
static volatile int64_t  synchronized_timestamp_offset = INT64_MIN;
const uint32_t           TIMER_STEPS                   = 50000;

#if configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC
static int64_t  previous_rx_real_timestamp         = 0;
static int64_t  previous_rx_monotonic_timestamp    = 0;
static uint32_t previous_transfer_id               = 0;
static enum { eSTATE_UPDATE, eSTATE_ADJUST } state = eSTATE_UPDATE;
static int32_t master_node_id                      = -1;

static void adjust(const uavcan_time_Synchronization_1_0* const message);
static void update(const CanardRxTransfer* const transfer);
#endif  // configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC

#if configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
// uavcan.pub.timekeeper_status: starcopter.TimekeeperStatus.0.1
volatile uint16_t timekeeper_status_subject_id     = SUBJECT_ID_DEFAULT;
static const char timekeeper_status_subject_type[] = starcopter_TimekeeperStatus_0_1_FULL_NAME_AND_VERSION_;
#endif  // configTIMEKEEPER_PUBLISH_STATUS_MESSAGE

void TIM3_IRQHandler(void) {
    if (PERIPH_BIT(TIM3->SR, TIM_SR_UIF_Pos)) {
        monotonic_timestamp_carry += TIMER_STEPS;
        PERIPH_BIT(TIM3->SR, TIM_SR_UIF_Pos) = 0;
    }
}

void init_timekeeper(void) {
    PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_TIM3EN_Pos) = 1;

    TIM3->PSC  = (SystemCoreClock / 1000000UL) - 1;  // 1 MHz
    TIM3->DIER = TIM_DIER_UIE;                       // Update Interrupt Enable
    TIM3->ARR  = TIMER_STEPS - 1;
    TIM3->CR1  = TIM_CR1_UIFREMAP | TIM_CR1_CEN;
    NVIC_SetPriority(TIM3_IRQn, configTIMEKEEPER_ISR_PRIORITY);
    NVIC_EnableIRQ(TIM3_IRQn);
}

uint64_t get_monotonic_timestamp_us(void) {
    __disable_irq();
    uint64_t carry   = monotonic_timestamp_carry;
    uint32_t counter = TIM3->CNT;
    __enable_irq();

    if (counter & TIM_CNT_UIFCPY) {
        /* The UIFCPY bit is set when an update event is pending, which has not yet been handled.
         * In this context this means the timer has overflown, but the global monotonic_timestamp_carry
         * has not yet been updated. To counter this (rare) race condition, we can update our local
         * carry value without side effects.
         */
        carry += TIMER_STEPS;
    }
    return carry + (counter & 0xFFFFUL);
}

uint64_t synchronize_timestamp(uint64_t monotonic_timestamp) {
    int64_t synchronized_timestamp = monotonic_timestamp + synchronized_timestamp_offset;
    return synchronized_timestamp >= 0 ? synchronized_timestamp : 0;
}

uint64_t get_synchronized_timestamp_us(void) { return synchronize_timestamp(get_monotonic_timestamp_us()); }

uint64_t unwrap_partial_past_timestamp(uint16_t partial_timestamp) {
    __disable_irq();
    /* Inside this __disable_irq()...__enable_irq() block the monotonic_timestamp_carry is guaranteed not to change.
     * Should the timer wrap around just after sampling the carry value, it will be visible in the counter's UIFCPY bit.
     */
    uint64_t carry   = monotonic_timestamp_carry;
    uint32_t counter = TIM3->CNT;
    __enable_irq();
    if (counter & TIM_CNT_UIFCPY) carry += TIMER_STEPS;

    uint64_t ts = carry + partial_timestamp;
    if ((uint16_t) counter < partial_timestamp) {
        // the counter has wrapped around since partial_timestamp
        ts -= TIMER_STEPS;
    }
    return ts;
}

void adjust_synchronized_timestamp_offset(int64_t error) { synchronized_timestamp_offset -= error; }

#if configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC
// RxTransferHandler
CanardRxTransfer* handle_time_synchronization_transfer(const CanardRxTransfer* const transfer,
                                                       const NodeSubscription* const sub) {
    ASSERT(transfer->metadata.port_id == uavcan_time_Synchronization_1_0_FIXED_PORT_ID_);

    uavcan_time_Synchronization_1_0 message;
    size_t                          size = transfer->payload_size;
    const int_fast8_t               deserialization_status =
        uavcan_time_Synchronization_1_0_deserialize_(&message, transfer->payload, &size);
    ASSERT(deserialization_status == 0);

    const uint64_t time_since_previous_msg = transfer->timestamp_usec - previous_rx_monotonic_timestamp;
    const bool     needs_init              = (master_node_id < 0);
    const bool     switch_master           = transfer->metadata.remote_node_id < master_node_id;
    const bool     publisher_timed_out     = time_since_previous_msg > 3000000;  // 3.0 s

    if (needs_init || switch_master || publisher_timed_out) {
        update(transfer);
    } else if (transfer->metadata.remote_node_id == master_node_id) {
        if (state == eSTATE_ADJUST) {
            const bool msg_invalid = message.previous_transmission_timestamp_microsecond == 0;
            const bool wrong_tid =
                transfer->metadata.transfer_id != ((previous_transfer_id + 1) & CANARD_TRANSFER_ID_MAX);
            const bool wrong_timing = time_since_previous_msg > 1500000;  // 1.5 s, TODO: dynamic
            if (msg_invalid || wrong_tid || wrong_timing) {
                state = eSTATE_UPDATE;
            }
        }

        switch (state) {
            case eSTATE_ADJUST:
                adjust(&message);
                break;
            case eSTATE_UPDATE:
                update(transfer);
                break;
        }
    }

#    if configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
    static uint_fast8_t transfer_id = 0;
    const uint16_t      subject_id  = timekeeper_status_subject_id;
    if (subject_id <= CANARD_SUBJECT_ID_MAX) {
        const starcopter_TimekeeperStatus_0_1 obj = {
            .previous_sync_timestamp_us.microsecond = message.previous_transmission_timestamp_microsecond,
            .timesync_master_node_id                = master_node_id,
            .previous_local_timestamp_us            = transfer->timestamp_usec,
            .synchronized_timestamp_offset_us       = synchronized_timestamp_offset,
            .tim3_auto_reload_register              = TIM3->ARR,
        };

        size_t   buffer_size = starcopter_TimekeeperStatus_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
        uint8_t* buf         = pvPortMalloc(buffer_size);
        ASSERT(buf);

        const int_fast8_t result = starcopter_TimekeeperStatus_0_1_serialize_(&obj, buf, &buffer_size);
        ASSERT(result == NUNAVUT_SUCCESS);

        const CanardTransferMetadata meta = {.priority       = CanardPriorityOptional,
                                             .transfer_kind  = CanardTransferKindMessage,
                                             .port_id        = subject_id,
                                             .remote_node_id = CANARD_NODE_ID_UNSET,
                                             .transfer_id    = transfer_id++};

        node_publish(100000ul, &meta, buffer_size, buf);
        vPortFree(buf);
    }
#    endif  // configTIMEKEEPER_PUBLISH_STATUS_MESSAGE

    return (CanardRxTransfer*) transfer;
}

static void adjust(const uavcan_time_Synchronization_1_0* const message) {
    const int32_t   RESET_FILTER            = INT32_MIN;
    static int32_t  last_adjustment_value   = RESET_FILTER;
    static int64_t  last_regular_adjustment = 0;
    static uint32_t glitch_count            = 9001;
    const int64_t   local_time_phase_error =
        previous_rx_real_timestamp - message->previous_transmission_timestamp_microsecond;

    if (local_time_phase_error < -2000000 || local_time_phase_error > 4000000) {
        // big adjustment
        const int32_t error_seconds = local_time_phase_error / 1000000ul;
        adjust_synchronized_timestamp_offset(local_time_phase_error);
        LOG_TRACE("clock adjusted by %li s", error_seconds);
        last_adjustment_value = RESET_FILTER;
        glitch_count          = 0;
    } else if (local_time_phase_error <= -100000 || local_time_phase_error >= 100000ul) {
        const int32_t error_milliseconds = local_time_phase_error / 1000;
        if (++glitch_count > 3) {
            adjust_synchronized_timestamp_offset(local_time_phase_error);
            LOG_TRACE("clock adjusted by %li ms", error_milliseconds);
            last_adjustment_value = RESET_FILTER;
            glitch_count          = 0;
        } else {
            LOG_DEBUG("ignoring Timesync glitch (?) of %li ms", error_milliseconds);
            last_regular_adjustment = previous_rx_monotonic_timestamp;
        }
    } else {
        static uint32_t integrator = TIMER_STEPS << 16;
        if (last_adjustment_value == RESET_FILTER) {
            last_adjustment_value = local_time_phase_error;
        } else {
            const float k         = 1.f / 4;
            last_adjustment_value = k * local_time_phase_error + (1 - k) * last_adjustment_value;

            const float dt_ms  = (previous_rx_monotonic_timestamp - last_regular_adjustment) / 1000;
            const float I_gain = .5f;
            integrator += I_gain * dt_ms * local_time_phase_error;
            TIM3->ARR = integrator >> 16;
        }
        // small adjustment
        adjust_synchronized_timestamp_offset(last_adjustment_value);
        LOG_TRACE("clock adjusted by %4li us (err %4li us, arr %u)", last_adjustment_value,
                  (int32_t) local_time_phase_error, (uint16_t) (integrator >> 16));
        last_regular_adjustment = previous_rx_monotonic_timestamp;

        glitch_count = 0;

        // volatile uint8_t* const HSITRIM = (uint8_t*) (RCC_BASE + 7);
    }

    state = eSTATE_UPDATE;
}

static void update(const CanardRxTransfer* const transfer) {
    previous_rx_monotonic_timestamp = transfer->timestamp_usec;
    previous_rx_real_timestamp      = synchronized_timestamp_offset + transfer->timestamp_usec;
    master_node_id                  = transfer->metadata.remote_node_id;
    previous_transfer_id            = transfer->metadata.transfer_id;
    state                           = eSTATE_ADJUST;
}
#endif  // configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC

#if configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
// uavcan.pub.timekeeper_status.id
UAVCANRegisterDeclaration _reg_pub_timekeeper_status_id = {
    .name        = "uavcan.pub.timekeeper_status.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &timekeeper_status_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.timekeeper_status.type
UAVCANRegisterDeclaration _reg_pub_timekeeper_status_type = {
    .name        = "uavcan.pub.timekeeper_status.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(timekeeper_status_subject_type) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) timekeeper_status_subject_type,
    .update_hook = NULL,
    .lock        = NULL,
};
#endif  // configTIMEKEEPER_PUBLISH_STATUS_MESSAGE
