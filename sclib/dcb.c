/**
 * @file dcb.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Device Configuration Block Utilities
 * @version 0.2
 * @date 2021-07-13
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <stddef.h>

#include "dcb.h"
#include "memory_map.h"
#include "crc.h"

bool dcb_validate(void) {
    if (device_configuration_block.magic != DEVICE_CONFIG_MAGIC) return false;
    if (device_configuration_block.version != eDEV_CONFIG_VERSION_CURRENT) return false;

    const uint32_t bytes_to_crc = device_configuration_block.size - sizeof(uint32_t);
    uint32_t*      pcrc         = (uint32_t*) ((uint32_t) &device_configuration_block + bytes_to_crc);
    if (pcrc < &__flash_start__ || pcrc >= &__flash_end__) return false;

    uint32_t crc = *pcrc;
    return crc32((const uint32_t*) &device_configuration_block, bytes_to_crc, &crc);
}
