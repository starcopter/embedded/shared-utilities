/**
 * @file image.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Image Header Specification
 * @version 0.1
 * @date 2021-06-11
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <inttypes.h>
#include <stdbool.h>
#include <device.h>

#include "dcb.h"

typedef struct ELFNote {
    uint32_t namesz;
    uint32_t descsz;
    uint32_t type;
    uint8_t  data[];
} ELFNote_t;

static const uint32_t IMAGE_MAGIC = 0x7f << 0 | 's' << 8 | 'c' << 16 | '0' << 24;  // 0x3063737f

enum ImageHeaderVersion { eHEADER_VERSION_2 = 2, eHEADER_VERSION_3 = 3, eHEADER_VERSION_CURRENT = eHEADER_VERSION_3 };

/* Software Version Specification in Little-Endian Order, sortable as u32 */
typedef union SoftwareVersion {
    struct {
        const uint8_t commits;  // Git Commits after Version Tag, see `git describe`
        const uint8_t patch;    // Semantic Versioning
        const uint8_t minor;    // Semantic Versioning
        const uint8_t major;    // Semantic Versioning
    };
    uint32_t u32;  // sort accessor
} SWVersion_t;

typedef struct ImageHeaderV3 {
    /* header's header, this is expected not to change */
    const uint32_t        image_magic;   // should be IMAGE_MAGIC (0x3063737f)
    const uint32_t* const header_start;  // image base address, absolute
    const uint32_t* const vector_table;  // vector table address, absolute
    const uint32_t        header_size;   // total header size in bytes
    const uint32_t* const image_end;     // image end address (first non-image byte), absolute

    /* header's implementation specific body, (header_size - 24) bytes */
    const uint8_t     image_hdr_version;   // should be eHEADER_VERSION_CURRENT
    const uint8_t     _reserved;           // reserved
    const uint16_t    clean_build : 1;     // this build stems from a clean worktree (git)
    const uint16_t    _flags      : 15;    // reserved
    const SWVersion_t version;             // software version
    const SWVersion_t min_upgrade_from;    // earliest software version to safely upgrade from
    const SWVersion_t max_downgrade_from;  // latest software version to safely downgrade from
    const HWVersion_t target_hardware;     // target hardware version
    const HWVersion_t min_hardware;        // earliest compatible hardware version
    const uint32_t    time_utc;            // build (or commit/release) time UTC epoch
    const uint8_t     git_sha[20];         // git hash, byte by byte
    const char        name[52];            // null-terminated application name, as in uavcan.node.GetInfo.1.0.name

    /* header's tail, has to be the last word */
    const uint32_t header_crc;  // CRC of the header up to here
} ImageHeader_t;

/* Last word of an image, ENDS aligned at 8-byte boundary. Declared in Linker Script. */
extern uint32_t __image_crc;
/* First word AFTER the image; not necessarily accessible. Declared in Linker Script. */
extern uint32_t __image_end;

typedef volatile struct ApplicationContext {
    const ImageHeader_t* application;
    uint32_t             boot_count;
} ApplicationContext_t;

bool image_check(const ImageHeader_t* const header, bool check_header, bool check_body);

// maximum length of a version string, e.g. required buffer capacity
static const size_t VERSION_STRING_MAXSZ = sizeof("v255.255.255-255*");

/**
 * @brief Render Version Information into Human-Readable String
 *
 * @attention `out_version_string` has to have a capacity of at least VERSION_STRING_MAXSZ characters!
 *
 * @param out_version_string character buffer to write the version string into; at least VERSION_STRING_MAXSZ long
 * @param version version to render
 * @param clean_build clean build or not
 * @return int number of characters written
 */
int image_render_version_string(char* out_version_string, const SWVersion_t* const version, const bool clean_build);

void __NO_RETURN image_boot(ApplicationContext_t* const ctx);

/* ---------------- deprecated previous image header versions ---------------- */

struct __attribute__((packed, aligned(4))) ImageHeaderV2 {
    /* header's header, this is expected not to change */
    const uint32_t        image_magic;   // should be IMAGE_MAGIC (0x3063737f)
    const uint32_t* const header_start;  // image base address, absolute
    const uint32_t* const vector_table;  // vector table address, absolute
    const uint32_t        header_size;   // total header size in bytes
    const uint32_t* const image_end;     // image end address (first non-image byte), absolute

    /* header's implementation specific body, (header_size - 24) bytes */
    const uint8_t  image_hdr_version;  // eHEADER_VERSION_2
    const uint8_t  image_type;         // a value from the image_type_t enum
    const uint16_t _reserved;
    const uint8_t  version_major;      // semver
    const uint8_t  version_minor;      // semver
    const uint8_t  version_patch;      // semver
    const uint8_t  version_clean : 1;  // this build stems from a clean version tag (git)
    const uint8_t  clean_build   : 1;  // this build stems from a clean worktree (git)
    const uint8_t  _flags        : 6;  // reserved
    const uint8_t  git_sha[20];        // git hash, byte by byte
    const uint32_t time_utc;           // build (or commit/release) time UTC epoch
    const char     target_name[52];    // null-terminated target name, as in uavcan.node.GetInfo.1.0.name
    const char     hwv_major;          // target major hardware version, 'A' through 'Z' (Altium style)
    const uint8_t  hwv_minor;          // target minor hardware version, 1 through 255
    const char     hwv_min_major;      // earliest compatible major hardware version, 'A' through 'Z' (Altium style)
    const uint8_t  hwv_min_minor;      // earliest compatible minor hardware version, 1 through 255

    /* header's tail, has to be the last word */
    const uint32_t header_crc;  // CRC of the header up to here
};
