/**
 * @file crc.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief CRC Utilities
 * @version 0.1
 * @date 2021-07-13
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <device.h>

#include "crc.h"

bool crc32(const uint32_t* data, uint32_t len, uint32_t* in_out_crc) {
    // https://m0agx.eu/2021/04/09/matching-stm32-hardware-crc-with-standard-crc-32/
    CRC->CR = CRC_CR_RESET | CRC_CR_REV_OUT | (0b11 << CRC_CR_REV_IN_Pos);

    const uint32_t* const data_end = (uint32_t*) ((uint32_t) data + len);

    const uint32_t* word = data;
    while (word < data_end) {
        CRC->DR = *word++;
    }

    const uint32_t target_crc     = *in_out_crc;
    const uint32_t calculated_crc = ~CRC->DR;

    // clean up
    CRC->CR = CRC_CR_RESET;

    *in_out_crc = calculated_crc;
    return target_crc == calculated_crc;
}

void crc_init(void) {
    PERIPH_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN_Pos) = 1;
    (void) PERIPH_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN_Pos);
}

void crc_deinit(void) { PERIPH_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN_Pos) = 0; }
