/**
 * @file timekeeper.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Logic and States to keep a Monotonic Microsecond Clock
 * @version 0.1
 * @date 2021-12-02
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * The timekeeper module is hard-wired to use TIM3 as time source, which is the only timer connected to the STM32G4's
 * FDCAN module. This way, the Rx FIFO's or Tx event queue's timestamp can be referenced directly to this module's
 * monotonic timestamp.
 *
 * This module provides two microsecond-resolution timestamps: a monotonic timestamp, and a synchronized timestamp.
 *
 * The monotonic timestamp starts at zero, monotonically counts up and will never leap.
 * The synchronized timestamp can be adjusted to follow an external synchronization trigger signal.
 *
 * @attention The timekeeper.c source file implements the TIM3_IRQHandler function.
 */

#pragma once

#include <main.h>
#include <stdint.h>
#include "assert.h"
#include <canard/node.h>
#include <canard/canard.h>
#include <uavcan/time/Synchronization_1_0.h>

#ifndef configTIMEKEEPER_ISR_PRIORITY
#    define configTIMEKEEPER_ISR_PRIORITY 4
#endif
#ifndef configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC
#    define configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC 1
#endif

/**
 * @brief Initialize the Timekeeper Module
 *
 * This enables, configures and starts TIM3 as background time base.
 */
void init_timekeeper(void);

uint64_t get_monotonic_timestamp_us(void);

uint64_t get_synchronized_timestamp_us(void);

uint64_t unwrap_partial_past_timestamp(uint16_t partial_timestamp);

#if configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC
CanardRxTransfer* handle_time_synchronization_transfer(const CanardRxTransfer* const transfer,
                                                       const NodeSubscription* const sub);
#endif
