/**
 * @file platform.h
 * @author Lasse Fröhner (lasse@starcopter.com)
 * @brief platform introspection helpers
 * @version 0.1
 * @date 2023-01-22
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdint.h>

/**
 * @brief Display Platform Information
 *
 * If the respective log levels are enabled, this function will cause an output similar to the following:
 *
 *     NOTICE: com.starcopter.aeric.motherboard v0.4.2-3 on HW B.5
 *       INFO: Git Rev 76ebe893, Build ID 453c9296, CRC 5062ad04/2b79cfe5
 *       INFO: Running on device 26087fbd
 */
void print_platform_info(void);

/**
 * @brief Display Boot ID
 *
 * If the respective log levels are enabled, this function will cause an output similar to the following:
 *
 *      DEBUG: Boot ID df136a41df6a9c4a2fc42618a99b2114
 *
 * @param boot_id reference to (randomly created) boot ID
 */
void print_boot_id(const uint32_t boot_id[4]);
