/**
 * @file platform.c
 * @author Lasse Fröhner (lasse@starcopter.com)
 * @brief platform introspection helpers
 * @version 0.1
 * @date 2023-01-22
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "platform.h"
#include "image.h"
#include "dcb.h"

#define LOG_LEVEL LOG_LEVEL_DEBUG
#include "log.h"

void print_platform_info(void) {
    extern const ImageHeader_t image_hdr;
    const char* const          name = image_hdr.name;
    char                       image_version_string[VERSION_STRING_MAXSZ];
    image_render_version_string(image_version_string, &image_hdr.version, image_hdr.clean_build);
    const HWVersion_t* const hw = &device_configuration_block.release;
    LOG_NOTICE("%s %s on HW %c.%u%s", name, image_version_string, hw->major, hw->minor, dcb_validate() ? "" : " (?)");

    extern const ELFNote_t g_note_build_id;
    LOG_INFO("Git Rev %08lx%s, Build ID %08lx, CRC %08lx/%08lx", __builtin_bswap32(*(uint32_t*) &image_hdr.git_sha[0]),
             image_hdr.clean_build ? "" : "*",
             __builtin_bswap32(*(uint32_t*) &g_note_build_id.data[g_note_build_id.namesz]), image_hdr.header_crc,
             __image_crc);
    LOG_INFO("Running on device %08lx", __builtin_bswap32(*(uint32_t*) &device_configuration_block.uid[12]));
}

void print_boot_id(const uint32_t boot_id[4]) {
    LOG_DEBUG("Boot ID %08lx%08lx%08lx%08lx", boot_id[3], boot_id[2], boot_id[1], boot_id[0]);
}
