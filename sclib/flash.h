/**
 * @file flash.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Utilities to Program Internal Flash Memory
 * @version 0.1
 * @date 2021-07-11
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdint.h>

/**
 * @brief Unlock Integrated Flash Memory
 *
 * @details In normal operation the flash memory is write-protected and cannot be altered. Before pages can be erased
 *          and memory cells can be written to, the flash memory has to be unlocked.
 *
 * @attention The flash memory has to be in locked (default) state when calling this function.
 *            Two subsequent calls to flash_unlock() will cause a HardFault condition!
 *
 * @see flash_lock
 */
void flash_unlock(void);

/**
 * @brief Lock Integrated Flash Memory
 *
 * @details Restore flash memory to its original locked state, after it might have been previously unlocked.
 *          This function is safe to be called multiple times without unlocking the flash.
 *
 * @see flash_unlock
 */
void flash_lock(void);

/**
 * @brief Calculate Page Number of a Memory Address in Flash
 *
 * @param addr memory address in flash
 * @return uint32_t page number
 */
uint32_t flash_get_page_number(const uint32_t* const addr);

/**
 * @brief Erase a Single Page in Flash Memory
 *
 * @attention The flash memory has to be in unlocked state when calling this function.
 *
 * @param page page number to erase
 *
 * @see flash_unlock
 */
void flash_erase_page(uint32_t page);

/**
 * @brief Program Data into Flash Memory
 *
 * @details Copy double-words from the location pointed to by src (possibly in RAM) to the location in flash
 *          pointed to by dst. Both the destination address and the length in bytes have to be double-word aligned.
 *          The function interface is similar to that of the standard library's memcpy(), except for the return value.
 *
 * @attention The flash memory has to be in unlocked state when calling this function.
 *
 * @param dst flash memory address to write to; double-word (64-bit) aligned
 * @param src source address; word (32-bit) aligned
 * @param bytes number of bytes to write; has to be a multiple of 8
 *
 * @see flash_unlock flash_erase_page memcpy
 */
void flash_write(uint32_t* dst, uint32_t* src, uint32_t bytes);
