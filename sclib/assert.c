/**
 * @file assert.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Assert Implementation
 * @version 0.1
 * @date 2021-05-31
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <main.h>
#include "assert.h"
#include "console.h"

void assertion_failed(const char* file, uint32_t line) {
    printf("ALERT: ASSERTION FAILED in %s:%lu\r\n", file, line);
    if (DEBUGGER_CONNECTED) {
        console_flush();
        HALT();
    } else {
        printf("No debugger connected, will not halt\r\n");
    }
    printf("System will reset now.\r\n\r\n");
    console_flush();
    NVIC_SystemReset();
}
